const express = require('express');
const MongoClient = require('mongodb').MongoClient;
const bodyParser = require('body-parser');
const cors = require('cors');
const app = express();

app.use(cors());
app.use(bodyParser.json({ limit: '10mb', extended: true }));
app.use(bodyParser.urlencoded({ limit: '10mb', extended: true }));

app.get('/', function (req, res) {
    res.send('<b>/</b>')
});

// var base64Data = data.category_image.replace(/^data:image\/png;base64,/, "");
// fs.writeFile("uploads/property_images/" + data.property_name + ".png", base64Data, 'base64', function (err) {});

app.post('/saveProperty', function (req, res) {
    MongoClient.connect("mongodb://localhost:27017/", function (error, db) {
        if (error) throw error;
        var dbo = db.db('propertyapp');
        var propertyData = {
            image: req.body.image,
            address: req.body.address,
            price: req.body.price,
            propertyType: req.body.propertyType,
            description: req.body.description,
            number_of_beds: req.body.number_of_beds,
            number_of_baths: req.body.number_of_baths,
            area: req.body.area,
            user: req.body.user
        };
        dbo.collection('properties').insertOne(propertyData, function (error, response) {
            if (error) {
                res.json({ status: 'failed' });
            }
            else {
                res.json({ status: 'success' });
            }
            db.close();
        });
    });
});

app.get('/saveName', function (req, res) {
    MongoClient.connect("mongodb://localhost:27017/", function (connectionError, connectionObject) {
        if (connectionError) {
            console.log('UNABLE TO CONNECT TO MONGODB');
        }
        else {
            console.log('CONNECTED TO MONGODB');
            var dbo = connectionObject.db('propertyapp');
            var name = {
                first_name: req.query.firstname,
                last_name: req.query.lastname
            };

            dbo.collection('users').insertOne(name, function (error, response) {
                if (error) {
                    res.json({ status: 'failed' });
                }
                else {
                    res.json({ status: 'success' });
                }
            });
        }

    });
});

app.get('/getAllProperty', function (req, res) {
    MongoClient.connect("mongodb://localhost:27017/", function (error, db) {
        if (error) throw error;
        var dbo = db.db('propertyapp');
        dbo.collection('properties').find({}).toArray(function (error, result) {
            res.send(result);
        });
    });
});


app.post('/checkEmailAvailability', function (req, res) {
    MongoClient.connect("mongodb://localhost:27017/", function (error, db) {
        if (error) throw error;
        var dbo = db.db('propertyapp');

        var userDetails = {
            email: req.body.email
        };
        dbo.collection('users').find(userDetails).toArray(function (error, result) {
            res.json(result);
        });
    });
});


app.post('/checkUserLogin', function (req, res) {
    MongoClient.connect("mongodb://localhost:27017/", function (error, db) {
        if (error) throw error;
        var dbo = db.db('propertyapp');

        var userDetails = {
            email: req.body.email,
            password: req.body.password
        };
        dbo.collection('users').find(userDetails).toArray(function (error, result) {
            res.json(result);
        });
    });
});

app.post('/getUserInfo', function (req, res) {
    MongoClient.connect("mongodb://localhost:27017/", function (error, db) {
        if (error) throw error;
        var dbo = db.db('propertyapp');

        var userDetails = {
            email: req.body.email
        };
        dbo.collection('users').find(userDetails).toArray(function (error, result) {
            res.json(result);
        });
    });
});


app.post('/getMyProperties', function (req, res) {
    MongoClient.connect("mongodb://localhost:27017/", function (error, db) {
        if (error) throw error;
        var dbo = db.db('propertyapp');

        var userDetails = {
            user: req.body.email
        };
        dbo.collection('properties').find(userDetails).toArray(function (error, result) {
            res.json(result);
        });
    });
});


app.post('/deleteMyProperties', function (req, res) {
    MongoClient.connect("mongodb://localhost:27017/", function (error, db) {
        if (error) throw error;
        var dbo = db.db('propertyapp');

        dbo.collection('properties').deleteOne( req.body.property , (error, response) => {
            if (error) {
                res.json({ status: 'failed' });
            }
            else {
                res.json({ status: 'success' });
            }            
            db.close();
        });

    });
});



app.post('/saveUser', function (req, res) {
    MongoClient.connect("mongodb://localhost:27017/", function (error, db) {
        if (error) throw error;
        var dbo = db.db('propertyapp');
        dbo.collection('users').insertOne(req.body, function (error, response) {
            if (error) {
                res.json({ status: 'failed' });
            }
            else {
                res.json({ status: 'success' });
            }
            db.close();
        });
    });
});

app.listen(3000, () =>
    console.log('App Running On PORT : 3000')
);
